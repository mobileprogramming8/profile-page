import 'package:flutter/material.dart';

enum APP_THEME{LIGHT, DARK}

void main() {
  runApp(ContactProfilePage());
}

class MyAppTheme {
  static ThemeData appThemeLight () {
    return ThemeData(
        brightness: Brightness.light,
        appBarTheme: AppBarTheme(
          color: Colors.white,
          iconTheme: IconThemeData(
            color: Colors.black,
          ),
        ),
        iconTheme: IconThemeData(
          color: Colors.indigo.shade300,
        ),
    );
  }

  static ThemeData appThemeDark () {
    return ThemeData(
      brightness: Brightness.dark,
      appBarTheme: AppBarTheme(
        color: Colors.white,
        iconTheme: IconThemeData(
          color: Colors.black,
        ),
      ),
      iconTheme: IconThemeData(
        color: Colors.indigo.shade300,
      ),
    );
  }
}

class ContactProfilePage extends StatefulWidget {
  @override
  State<ContactProfilePage> createState() => _ContactProfilePageState();
}

class _ContactProfilePageState extends State<ContactProfilePage> {
  var currentTheme = APP_THEME.DARK;

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      theme: currentTheme == APP_THEME.LIGHT
          ? MyAppTheme.appThemeDark()
          : MyAppTheme.appThemeLight(),
      home: Scaffold(
        appBar: buildAppBarWidget(),
        body: buildBodyWidget(),
        floatingActionButton: FloatingActionButton(
          child: Icon(Icons.threesixty, color: Colors.white),
          backgroundColor: Colors.blue.shade300,
          onPressed: () {
            setState(() {
              currentTheme == APP_THEME.LIGHT
                  ? currentTheme = APP_THEME.DARK
                  : currentTheme = APP_THEME.LIGHT;
            });
          },
        ),
      ),
    );
  }
}

Widget buildCallButton() {
  return Column(
    children: <Widget>[
      IconButton(
        icon: Icon(
          Icons.call,
          // color: Colors.teal,
        ),
        onPressed: () {},
      ),
      Text("Call"),
    ],
  );
}

Widget buildTextButton() {
  return Column(
    children: <Widget>[
      IconButton(
        icon: Icon(
          Icons.message,
          // color: Colors.teal,
        ),
        onPressed: () {},
      ),
      Text("Text"),
    ],
  );
}

Widget buildVideoCallButton() {
  return Column(
    children: <Widget>[
      IconButton(
        icon: Icon(
          Icons.video_call,
          // color: Colors.teal,
        ),
        onPressed: () {},
      ),
      Text("Video"),
    ],
  );
}

Widget buildEmailButton() {
  return Column(
    children: <Widget>[
      IconButton(
        icon: Icon(
          Icons.email,
          // color: Colors.teal,
        ),
        onPressed: () {},
      ),
      Text("Email"),
    ],
  );
}

Widget buildDirectionsButton() {
  return Column(
    children: <Widget>[
      IconButton(
        icon: Icon(
          Icons.directions,
          // color: Colors.teal,
        ),
        onPressed: () {},
      ),
      Text("Directions"),
    ],
  );
}

Widget buildPayButton() {
  return Column(
    children: <Widget>[
      IconButton(
        icon: Icon(
          Icons.attach_money,
          // color: Colors.teal,
        ),
        onPressed: () {},
      ),
      Text("Pay"),
    ],
  );
}

AppBar buildAppBarWidget() {
  return AppBar(
    backgroundColor: Colors.teal,
    leading: Icon(
      Icons.arrow_back,
      color: Colors.white,
    ),
    actions: <Widget>[
      IconButton(
        icon: Icon(Icons.star_border),
        color: Colors.white,
        onPressed: () {
          print("Contact is starred");
        },
      ),
    ],
  );
}

Widget buildBodyWidget() {
  return ListView(children: <Widget>[
    Column(children: <Widget>[
      Container(
        width: double.infinity,
        height: 250,
        child: Image.network(
          "https://static1.cbrimages.com/wordpress/wp-content/uploads/2020/09/Attack-on-Titan-Levi-Ackerman-1.jpg?q=50&fit=contain&w=1140&h=&dpr=1.5",
          fit: BoxFit.cover,
        ),
      ),
      Container(
        height: 60,
        child: Row(
          mainAxisAlignment: MainAxisAlignment.start,
          children: <Widget>[
            Padding(
              padding: EdgeInsets.all(8.0),
              child: Text(
                "Levi Ackerman",
                style: TextStyle(fontSize: 30),
              ),
            ),
          ],
        ),
      ),
      Divider(
        color: Colors.teal,
      ),
      Container(
        margin: const EdgeInsets.only(top: 8, bottom: 8),
        child: Theme(
          data: ThemeData(
            iconTheme: IconThemeData(
              color: Colors.blue.shade300,
            ),
          ),
          child: profileActionItems(),
        ),
      ),
      Divider(color: Colors.teal),
      mobilePhoneListTile(),
      OtherPhoneListTile(),
      Divider(color: Colors.teal),
      emailListTile(),
      Divider(
        color: Colors.teal,
      ),
      addressListTile(),
      Divider(
        color: Colors.teal,
      ),
    ]),
  ]);
}

// ListTile
Widget mobilePhoneListTile() {
  return ListTile(
    leading: Icon(Icons.call, color: Colors.teal.shade200),
    title: Text("330-803-3390"),
    subtitle: Text("mobile"),
    trailing: IconButton(
      icon: Icon(Icons.message),
      color: Colors.teal,
      onPressed: () {},
    ),
  );
}

Widget OtherPhoneListTile() {
  return ListTile(
    leading: Icon(null),
    title: Text("440-440-3390"),
    subtitle: Text("mobile"),
    trailing: IconButton(
      icon: Icon(Icons.message),
      color: Colors.teal,
      onPressed: () {},
    ),
  );
}

Widget emailListTile() {
  return ListTile(
    leading: Icon(Icons.email, color: Colors.teal.shade200),
    title: Text("ackerman@levi.com"),
    subtitle: Text("work"),
  );
}

Widget addressListTile() {
  return ListTile(
    leading: Icon(Icons.location_pin, color: Colors.teal.shade200),
    title: Text("wall maria St, Titan"),
    subtitle: Text("home"),
    trailing: IconButton(
      icon: Icon(Icons.directions),
      color: Colors.teal,
      onPressed: () {},
    ),
  );
}

Widget profileActionItems(){
  return Row(
    mainAxisAlignment: MainAxisAlignment.spaceEvenly,
    children: <Widget>[
      buildCallButton(),
      buildTextButton(),
      buildVideoCallButton(),
      buildEmailButton(),
      buildDirectionsButton(),
      buildPayButton(),
    ],
  );
}